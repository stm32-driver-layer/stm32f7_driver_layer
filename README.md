# STM32F7_Driver_Layer

To get the project:
-------------------
Clone the repository: git clone git@gitlab.com:stm32-driver-layer/stm32f7_driver_layer.git

To check all available Gradle Tasks:
------------------------------------
	$ gradle tasks --all

Gradle Main Tasks:
-------------------
    $ gradle BuildDocumentation
    $ gradle BuildStubs
    $ gradle BuildTests

Gradle Main Tasks for STM32F769I_DISCO Board:
---------------------------------------------
    $ gradle BuildDebugForSTM32F769I_DISCO
    $ gradle BuildReleaseForSTM32F769I_DISCO
    $ gradle FullBuildForSTM32F769I_DISCO
    $ gradle FullBuildWithDocAndTestForSTM32F769I_DISCO
    $ gradle GenerateReleasePackageForSTM32F769I_DISCO
    $ gradle GenerateReleasePackageWithDocumentationForSTM32F769I_DISCO
    $ gradle installLibraryForSTM32F769I_DISCO
