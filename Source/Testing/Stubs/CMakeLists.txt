#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: STM32F7_Driver_Layer                                                #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

coloredMessage(BoldYellow "Loading Stubs Test Source CMakeLists" STATUS)

set(STUBS_TEST_APP_NAME ${PROJECT_FULLNAME}-Stubs)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

check_gtest_gmock_dependency()

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY})

add_subdirectory(STM32F7xx_HAL_Driver)
add_subdirectory(MbedOS)
add_subdirectory(BSP)

#############################################
#   A.1. Set Options                        #
#############################################

set(SOURCES ${Stubs_STM32F7xx_HAL_Driver_Sources}
            ${Stubs_MbedOS_Sources}
            ${Stubs_BSP_Sources})

set(HEADERS ${Stubs_STM32F7xx_HAL_Driver_Headers}
            ${Stubs_MbedOS_Headers}
            ${Stubs_BSP_Headers})

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################

add_library(${STUBS_TEST_APP_NAME} STATIC
            ${SOURCES})
