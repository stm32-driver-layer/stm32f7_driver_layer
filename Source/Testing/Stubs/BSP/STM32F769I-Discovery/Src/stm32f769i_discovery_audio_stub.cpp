/**
 ******************************************************************************
 * @file    stm32f769i_discovery_audio_stub.cpp
 * @author  Leonardo Winter Pereira
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/BSP/STM32F769I-Discovery/Inc/stm32f769i_discovery_audio_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Stubs
{
   namespace BSP
   {
      namespace STM32F769I_DISCOVERY
      {
         static I_STM32F769I_Discovery_Audio* p_STM32F769I_Discovery_Audio_Impl = NULL;

         const char* STM32F769I_Discovery_Audio_StubImplNotSetException::what(void) const throw()
         {
            return "No stub implementation is set to handle STM32F769I_Discovery_Audio functions";
         }

         void stub_setImpl(I_STM32F769I_Discovery_Audio* pNewImpl)
         {
            p_STM32F769I_Discovery_Audio_Impl = pNewImpl;
         }

         static I_STM32F769I_Discovery_Audio* STM32F769I_Discovery_Audio_Stub_Get_Impl(void)
         {
            if(p_STM32F769I_Discovery_Audio_Impl == NULL)
            {
               std::cerr << "ERROR: No Stub implementation is currently set to handle "
                         << "calls to STM32F769I_Discovery_Audio functions. Did you forget"
                         << "to call Stubs::BSP::STM32F769I_Discovery::stub_setImpl() ?" << std::endl;

               throw STM32F769I_Discovery_Audio_StubImplNotSetException();
            }

            return p_STM32F769I_Discovery_Audio_Impl;
         }

            /////////////////////////////////////////////////////
            // I_STM32F769I_Discovery_Audio Methods Definition //
            /////////////////////////////////////////////////////

         I_STM32F769I_Discovery_Audio::~I_STM32F769I_Discovery_Audio()
         {
            if(p_STM32F769I_Discovery_Audio_Impl == this)
            {
               p_STM32F769I_Discovery_Audio_Impl = NULL;
            }
         }

         /////////////////////////////////////////////////////////
         // _Mock_STM32F769I_Discovery_Audio Methods Definition //
         /////////////////////////////////////////////////////////

         _Mock_STM32F769I_Discovery_Audio::_Mock_STM32F769I_Discovery_Audio()
         {
         }
      }
   }
}

               ////////////////////////////////////////////
               // Definition of C functions to be mocked //
               ////////////////////////////////////////////

uint8_t BSP_AUDIO_OUT_Init(uint16_t OutputDevice,
                           uint8_t Volume,
                           uint32_t AudioFreq)
{
   return Stubs::BSP::STM32F769I_DISCOVERY::STM32F769I_Discovery_Audio_Stub_Get_Impl()->BSP_AUDIO_OUT_Init(OutputDevice,
                                                                                                           Volume,
                                                                                                           AudioFreq);
}

/*
void BSP_AUDIO_OUT_DeInit(void)
{

}

uint8_t BSP_AUDIO_OUT_Play(uint16_t* pBuffer,
            uint32_t Size)
{

}

void BSP_AUDIO_OUT_ChangeBuffer(uint16_t *pData,
                 uint16_t Size)
{

}

uint8_t BSP_AUDIO_OUT_Pause(void)
{

}

uint8_t BSP_AUDIO_OUT_Resume(void)
{

}

uint8_t BSP_AUDIO_OUT_Stop(uint32_t Option)
{

}

uint8_t BSP_AUDIO_OUT_SetVolume(uint8_t Volume)
{

}

void BSP_AUDIO_OUT_SetFrequency(uint32_t AudioFreq)
{

}

void BSP_AUDIO_OUT_SetAudioFrameSlot(uint32_t AudioFrameSlot)
{

}

uint8_t BSP_AUDIO_OUT_SetMute(uint32_t Cmd)
{

}

uint8_t BSP_AUDIO_OUT_SetOutputMode(uint8_t Output)
{

}

void BSP_AUDIO_OUT_TransferComplete_CallBack(void)
{

}

void BSP_AUDIO_OUT_HalfTransfer_CallBack(void)
{

}

void BSP_AUDIO_OUT_Error_CallBack(void)
{

}

void BSP_AUDIO_OUT_ClockConfig(SAI_HandleTypeDef *hsai,
                uint32_t AudioFreq,
                void *Params)
{

}

void BSP_AUDIO_OUT_MspInit(SAI_HandleTypeDef *hsai,
            void *Params)
{

}

void BSP_AUDIO_OUT_MspDeInit(SAI_HandleTypeDef *hsai,
              void *Params)
{

}

uint8_t BSP_AUDIO_IN_Init(uint32_t AudioFreq,
           uint32_t BitRes,
           uint32_t ChnlNbr)
{

}

uint8_t BSP_AUDIO_IN_InitEx(uint16_t InputDevice,
             uint32_t AudioFreq,
             uint32_t BitRes,
             uint32_t ChnlNbr)
{

}

uint8_t BSP_AUDIO_IN_AllocScratch(int32_t *pScratch,
                   uint32_t size)
{

}

uint8_t BSP_AUDIO_IN_GetChannelNumber(void)
{

}

void BSP_AUDIO_IN_DeInit(void)
{

}

uint8_t BSP_AUDIO_IN_Record(uint16_t *pData,
             uint32_t Size)
{

}

uint8_t BSP_AUDIO_IN_Stop(void)
{

}

uint8_t BSP_AUDIO_IN_Pause(void)
{

}

uint8_t BSP_AUDIO_IN_Resume(void)
{

}

void BSP_AUDIO_IN_TransferComplete_CallBack(void)
{

}

void BSP_AUDIO_IN_HalfTransfer_CallBack(void)
{

}

void BSP_AUDIO_IN_Error_CallBack(void)
{

}

void BSP_AUDIO_IN_ClockConfig(DFSDM_Filter_HandleTypeDef *hdfsdm_filter,
               uint32_t AudioFreq,
               void *Params)
{

}

void BSP_AUDIO_IN_MspInit(void)
{

}

void BSP_AUDIO_IN_MspDeInit(void)
{

}
*/
