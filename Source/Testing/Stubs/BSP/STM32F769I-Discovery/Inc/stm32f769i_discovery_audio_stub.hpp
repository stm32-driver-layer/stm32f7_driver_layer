/**
 ******************************************************************************
 * @file    stm32f769i_discovery_audio_stub.hpp
 * @author  Leonardo Winter Pereira
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef STM32F769I_DISCOVERY_AUDIO_STUBS_HPP
#define STM32F769I_DISCOVERY_AUDIO_STUBS_HPP

#include <Production/BSP/STM32F769I-Discovery/Inc/stm32f769i_discovery_audio.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Stubs
{
   namespace BSP
   {
      namespace STM32F769I_DISCOVERY
      {
         class STM32F769I_Discovery_Audio_StubImplNotSetException : public std::exception
         {
            public:
               virtual const char* what(void) const throw();
         };

         class I_STM32F769I_Discovery_Audio
         {
            public:
               virtual ~I_STM32F769I_Discovery_Audio();

               virtual uint8_t BSP_AUDIO_OUT_Init(uint16_t OutputDevice,
                                                  uint8_t Volume,
                                                  uint32_t AudioFreq) = 0;

               /*virtual void BSP_AUDIO_OUT_DeInit(void) = 0;

               virtual uint8_t BSP_AUDIO_OUT_Play(uint16_t* pBuffer,
                                                  uint32_t Size) = 0;

               virtual void BSP_AUDIO_OUT_ChangeBuffer(uint16_t *pData,
                                                       uint16_t Size) = 0;

               virtual uint8_t BSP_AUDIO_OUT_Pause(void) = 0;

               virtual uint8_t BSP_AUDIO_OUT_Resume(void) = 0;

               virtual uint8_t BSP_AUDIO_OUT_Stop(uint32_t Option) = 0;

               virtual uint8_t BSP_AUDIO_OUT_SetVolume(uint8_t Volume) = 0;

               virtual void BSP_AUDIO_OUT_SetFrequency(uint32_t AudioFreq) = 0;

               virtual void BSP_AUDIO_OUT_SetAudioFrameSlot(uint32_t AudioFrameSlot) = 0;

               virtual uint8_t BSP_AUDIO_OUT_SetMute(uint32_t Cmd) = 0;

               virtual uint8_t BSP_AUDIO_OUT_SetOutputMode(uint8_t Output) = 0;

               virtual void BSP_AUDIO_OUT_TransferComplete_CallBack(void) = 0;

               virtual void BSP_AUDIO_OUT_HalfTransfer_CallBack(void) = 0;

               virtual void BSP_AUDIO_OUT_Error_CallBack(void) = 0;

               virtual void BSP_AUDIO_OUT_ClockConfig(SAI_HandleTypeDef *hsai,
                                                      uint32_t AudioFreq,
                                                      void *Params) = 0;

               virtual void BSP_AUDIO_OUT_MspInit(SAI_HandleTypeDef *hsai,
                                                  void *Params) = 0;

               virtual void BSP_AUDIO_OUT_MspDeInit(SAI_HandleTypeDef *hsai,
                                                    void *Params) = 0;

               virtual uint8_t BSP_AUDIO_IN_Init(uint32_t AudioFreq,
                                                 uint32_t BitRes,
                                                 uint32_t ChnlNbr) = 0;

               virtual uint8_t BSP_AUDIO_IN_InitEx(uint16_t InputDevice,
                                                   uint32_t AudioFreq,
                                                   uint32_t BitRes,
                                                   uint32_t ChnlNbr) = 0;

               virtual uint8_t BSP_AUDIO_IN_AllocScratch(int32_t *pScratch,
                                                         uint32_t size) = 0;

               virtual uint8_t BSP_AUDIO_IN_GetChannelNumber(void) = 0;

               virtual void BSP_AUDIO_IN_DeInit(void) = 0;

               virtual uint8_t BSP_AUDIO_IN_Record(uint16_t *pData,
                                                   uint32_t Size) = 0;

               virtual uint8_t BSP_AUDIO_IN_Stop(void) = 0;

               virtual uint8_t BSP_AUDIO_IN_Pause(void) = 0;

               virtual uint8_t BSP_AUDIO_IN_Resume(void) = 0;

               virtual void BSP_AUDIO_IN_TransferComplete_CallBack(void) = 0;

               virtual void BSP_AUDIO_IN_HalfTransfer_CallBack(void) = 0;

               virtual void BSP_AUDIO_IN_Error_CallBack(void) = 0;

               virtual void BSP_AUDIO_IN_ClockConfig(DFSDM_Filter_HandleTypeDef *hdfsdm_filter,
                                                     uint32_t AudioFreq,
                                                     void *Params) = 0;

               virtual void BSP_AUDIO_IN_MspInit(void) = 0;

               virtual void BSP_AUDIO_IN_MspDeInit(void) = 0;*/
         };

         class _Mock_STM32F769I_Discovery_Audio : public I_STM32F769I_Discovery_Audio
         {
            public:
               _Mock_STM32F769I_Discovery_Audio();

               MOCK_METHOD3(BSP_AUDIO_OUT_Init, uint8_t (uint16_t, uint8_t, uint32_t));
         };

         typedef ::testing::NiceMock<_Mock_STM32F769I_Discovery_Audio> Mock_STM32F769I_Discovery_Audio;

         void stub_setImpl(I_STM32F769I_Discovery_Audio* pNewImpl);
      }
   }
}

#endif
