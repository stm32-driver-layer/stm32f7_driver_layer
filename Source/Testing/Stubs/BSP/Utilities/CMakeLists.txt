#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: STM32F7_Driver_Layer                                                #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

coloredMessage(BoldYellow "Loading Stubs BSP Utilities CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_SRC "${PROJECT_STUB_TESTING_SOURCE_FOLDER}/BSP/Utilities/Src")
set(DIRECTORY_NAME_INC "${PROJECT_STUB_TESTING_SOURCE_FOLDER}/BSP/Utilities/Inc")

#############################################
#   A.1. Set Options                        #
#############################################

set(Stubs_Utilities_Sources     #${DIRECTORY_NAME_SRC}/Fonts/font8.c
                                PARENT_SCOPE)

set(Stubs_Utilities_Headers PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################
