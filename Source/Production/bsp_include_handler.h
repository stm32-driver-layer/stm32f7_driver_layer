/*
 * bsp_include_handler.h
 *
 *  Created on: Oct 8, 2019
 *      Author: leowpereira
 */

#ifndef SOURCE_PRODUCTION_BSP_BSP_INCLUDE_HANDLER_H_
#define SOURCE_PRODUCTION_BSP_BSP_INCLUDE_HANDLER_H_

#define DECORATE(x)             <x>
#define MAKE_PATH(root, file)   DECORATE(root file)

#ifdef USE_STM32F769I_DISCO
#define INCLUDE_BSP(file)       MAKE_PATH(Production/BSP/STM32F769I-Discovery/Inc/stm32f769i_discovery_, file)
#else
#error "There is no BSP Folder for the current defined board platform"
#endif

#endif // SOURCE_PRODUCTION_BSP_BSP_INCLUDE_HANDLER_H_
