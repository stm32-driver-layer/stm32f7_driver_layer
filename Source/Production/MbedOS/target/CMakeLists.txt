#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: STM32F7_Driver_Layer                                                #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

coloredMessage(BoldYellow "Loading MbedOS target CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_SRC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/MbedOS/target/Src")
set(DIRECTORY_NAME_INC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/MbedOS/target/Inc")

#############################################
#   A.1. Set Options                        #
#############################################

set(target_Sources  ${DIRECTORY_NAME_SRC}/analogin_api.c
                    ${DIRECTORY_NAME_SRC}/analogout_api.c
                    ${DIRECTORY_NAME_SRC}/analogout_device.c
                    ${DIRECTORY_NAME_SRC}/can_api.c
                    ${DIRECTORY_NAME_SRC}/flash_api.c
                    ${DIRECTORY_NAME_SRC}/gpio_api.c
                    ${DIRECTORY_NAME_SRC}/gpio_irq_api.c
                    ${DIRECTORY_NAME_SRC}/gpio_irq_device.c
                    ${DIRECTORY_NAME_SRC}/hal_tick_16b.c
                    ${DIRECTORY_NAME_SRC}/hal_tick_32b.c
                    ${DIRECTORY_NAME_SRC}/i2c_api.c
                    ${DIRECTORY_NAME_SRC}/lp_ticker.c
                    ${DIRECTORY_NAME_SRC}/pinmap.c
                    ${DIRECTORY_NAME_SRC}/port_api.c
                    ${DIRECTORY_NAME_SRC}/pwmout_api.c
                    ${DIRECTORY_NAME_SRC}/pwmout_device.c
                    ${DIRECTORY_NAME_SRC}/rtc_api.c
                    ${DIRECTORY_NAME_SRC}/serial_api.c
                    ${DIRECTORY_NAME_SRC}/serial_device.c
                    ${DIRECTORY_NAME_SRC}/sleep.c
                    ${DIRECTORY_NAME_SRC}/spi_api.c
                    ${DIRECTORY_NAME_SRC}/stm_spi_api.c
                    ${DIRECTORY_NAME_SRC}/trng_api.c
                    ${DIRECTORY_NAME_SRC}/us_ticker_16b.c
                    ${DIRECTORY_NAME_SRC}/us_ticker_32b.c
                    PARENT_SCOPE)

set(target_Headers  PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################

