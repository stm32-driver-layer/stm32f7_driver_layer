/* mbed Microcontroller Library
 *******************************************************************************
 * Copyright (c) 2016, STMicroelectronics
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************
 */
#ifndef MBED_OBJECTS_H
#define MBED_OBJECTS_H

#include <Production/MbedOS/target/Inc/cmsis.h>

#ifdef USE_STM32F769I_DISCO
#include <Production/BSP/STM32F769I-Discovery/Inc/PeripheralNames.h>
#include <Production/BSP/STM32F769I-Discovery/Inc/PinNames.h>
#elif USE_STM32746G_DISCO
#include <Production/BSP/STM32746G-Discovery/Inc/PinNames.h>
#include <Production/BSP/STM32746G-Discovery/Inc/PeripheralNames.h>
#endif

#include <Production/MbedOS/target/Inc/PortNames.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gpio_irq_s
{
   IRQn_Type irq_n;
   uint32_t irq_index;
   uint32_t event;
   PinName pin;
};

struct port_s
{
   PortName port;
   uint32_t mask;
   PinDirection direction;
   __IO uint32_t *reg_in;
   __IO uint32_t *reg_out;
};

struct trng_s
{
   RNG_HandleTypeDef handle;
};

#include "common_objects.h"

#ifdef __cplusplus
}
#endif

#endif
