/*
 * hal_include_handler.h
 *
 *  Created on: Oct 8, 2019
 *      Author: leowpereira
 */

#ifndef SOURCE_PRODUCTION_STM32F7XX_HAL_DRIVER_HAL_INCLUDE_HANDLER_H_
#define SOURCE_PRODUCTION_STM32F7XX_HAL_DRIVER_HAL_INCLUDE_HANDLER_H_

#define DECORATE(x)             <x>
#define MAKE_PATH(root, file)   DECORATE(root file)

#ifdef MIDDLEWARE_USE_STM32F7
#define INCLUDE_HAL(file)       MAKE_PATH(Production/STM32F7xx_HAL_Driver/Inc/stm32f7xx_, file)
#else
#error "There is no default HAL (DEFAULT_MIDDLEWARE) defined for this project"
#endif

#endif // SOURCE_PRODUCTION_STM32F7XX_HAL_DRIVER_HAL_INCLUDE_HANDLER_H_
