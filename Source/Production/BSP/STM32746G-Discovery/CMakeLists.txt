#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: STM32F7_Driver_Layer                                                #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

coloredMessage(BoldYellow "Loading BSP STM32746G-Discovery CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_SRC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/BSP/STM32746G-Discovery/Src")
set(DIRECTORY_NAME_INC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/BSP/STM32746G-Discovery/Inc")

#############################################
#   A.1. Set Options                        #
#############################################

set(STM32746G_Discovery_Platform_Sources    ${DIRECTORY_NAME_SRC}/PeripheralPins.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery_audio.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery_camera.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery_eeprom.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery_lcd.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery_qspi.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery_sd.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery_sdram.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery_ts.c
                                            ${DIRECTORY_NAME_SRC}/stm32746g_discovery.c
                                            PARENT_SCOPE)

set(STM32746G_Discovery_Platform_Headers    PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################
